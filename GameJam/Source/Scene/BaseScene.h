#pragma once
#include <memory>
#include "Game/GameController.h"
class BaseScene;
using Unique_Base = std::unique_ptr<BaseScene>;


class BaseScene
{
public:
	virtual ~BaseScene();
	virtual Unique_Base Update(Unique_Base Own,const GameController &GameControllerObj)=0;		// 純粋仮想関数　
private:
	virtual int Init(void)=0;																	// 初期化用仮想関数
};


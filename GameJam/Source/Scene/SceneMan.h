#pragma once
#include <mutex>
#include <list>
#include <memory>
#include "VECTOR2.h"
#include "Game/Func/ListObj.h"
#include "BaseScene.h"

#define lpSceneMng SceneMan::GetInstans()

class Obj;
class GameController;

class SceneMan
{
public:

	static SceneMan& GetInstans(void)
	{
		static SceneMan s_SceneManInstance;
		return s_SceneManInstance;
	}

	VECTOR2 GetScreenSize();								// スクリーンサイズのゲッター	

	bool SetDrawOffSet(VECTOR2 vec);						// オフセットのセッター
	const VECTOR2 GetDrawOffSet(void);						// オフセットのゲッター

	void Run(void);											// ActiveScene呼び出し

	const VECTOR2 GetMapSize(void);							// MapSizeのゲッター
	const VECTOR2 GetGridSize(void);						// GridSizeのゲッター
private:
	SceneMan();
	~SceneMan();
	

	VECTOR2 DrawOffset;										// オフセットの保存用
	VECTOR2 MapSize;										// MAPのグリッド数を保存

	bool SysInit(void);										// システム全体の初期化

	std::unique_ptr<GameController> GameControllerObj;		// ゲームコントローラーのオブジェクト
	std::unique_ptr<BaseScene> ActiveScenePtr;				// 実行されるシーンを格納するユニークポインタ
};


#include "TitleScene.h"
#include "GameScene.h"
#include "SceneMan.h"
#include "ResourceManager/SoundMan.h"

#define SCREEN_OFFSET_X 40
#define SCREEN_OFFSET_Y 75

#define TITLE_MENU_CHIP_X 288
#define TITLE_MENU_CHIP_Y 72

#define TITLE_GUIDE_SIZE_X 150
#define TITLE_GUIDE_SIZE_Y 96

TitleScene::TitleScene()
{
	Init();
}


TitleScene::~TitleScene()
{
}

Unique_Base TitleScene::Update(Unique_Base Own, const GameController & GameContObj)
{
	ViewRulesOldFlag = ViewRulesFlag;

	// ゲーム本編へ移動
	if (GameContObj.GetInputDown(KEY_INPUT_RETURN) || SceneChangeFlag)
	{
		SceneChangeFlag = true;

		if (animEndFlag)
		{

			if (Mode == static_cast<unsigned int>(PLAYER_NUM::START))
			{
				if (oneloop)
				{
					lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("select01", "wav")[0], true);
					oneloop = false;
				}


				if (CheckSceneMove())
				{
					StopSoundMem(lpSoundMan.GetSoundId("title01", "wav")[0]);
					StopSoundMem(lpSoundMan.GetSoundId("select01", "wav")[0]);

					// ｹﾞｰﾑｼｰﾝ移行
					SetDrawBright(255, 255, 255);
					return std::make_unique<GameScene>();
				}
			}
			else
			{
				// ﾙｰﾙ表示
				lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("select01", "wav")[0], true);
				ViewRulesFlag = true;
				SceneChangeFlag = false;
			}
		}
		else
		{
			SceneChangeFlag = false;
		}
	}

	if ((SceneChangeFlag && animEndFlag) == false)
	{
		auto ModeChange = [&](auto key)
		{
			if (GameContObj.GetInputDown(key))
			{
				if (Mode == static_cast<unsigned int>(PLAYER_NUM::START))
				{
					Mode = static_cast<unsigned int>(PLAYER_NUM::RULES);
					return true;
				}
				else
				{
					Mode = static_cast<unsigned int>(PLAYER_NUM::START);
					return true;
				}
				return false;
			}
			
			return false;
		};
		
		if (ViewRulesFlag == false)
		{
			if (ModeChange(KEY_INPUT_UP) || ModeChange(KEY_INPUT_DOWN))
			{
				lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("cursor01", "wav")[0], true);
			}
			else
			{
				if (GameContObj.GetInputUp(KEY_INPUT_UP) || GameContObj.GetInputUp(KEY_INPUT_DOWN))
				{
					StopSoundMem(lpSoundMan.GetSoundId("cursor01", "wav")[0]);
				}
			}
		}
	}

	if (ViewRulesOldFlag)
	{
		// ﾙｰﾙを開いている時にｷｰが押されたら閉じる
		if (GameContObj.GetInputDown(KEY_INPUT_RETURN))
		{
			ViewRulesFlag = false;
		}
	}

	TitleScene::Draw();

	return std::move(Own);
}

int TitleScene::Init(void)
{

	lpSceneMng.SetDrawOffSet(VECTOR2(SCREEN_OFFSET_X, SCREEN_OFFSET_Y));					// OffSetを登録
	lpImageMan.GetImageId("title");
	lpImageMan.GetImageId("titlelogo");
	lpImageMan.GetImageId("titleguide");
	lpImageMan.GetImageId("rules");
	lpImageMan.GetImageId("titlemenu", { TITLE_MENU_CHIP_X, TITLE_MENU_CHIP_Y }, { 1,3 });
	lpImageMan.GetImageId("titlecursor");

	TitleAnimEnd	= false;
	animEndFlag		= false;
	SceneChangeFlag = false;
	ViewRulesFlag	= false;
	oneloop		= true;
	animCnt		= 0;
	sceneCnt	= 255;
	Mode		= static_cast<int>(PLAYER_NUM::START);

	return 0;
}

void TitleScene::Draw()
{
	ClsDrawScreen();						// 画面消去

	
	if (!ViewRulesFlag)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, animCnt);

		VECTOR2 offset = lpSceneMng.GetDrawOffSet();
		VECTOR2 screen = lpSceneMng.GetScreenSize();

		DrawGraph(0, 0, IMAGE_ID("title")[0], true);
		DrawGraph(offset.x, offset.y, IMAGE_ID("titlelogo")[0], true);

		DrawGraph(0, screen.y - TITLE_GUIDE_SIZE_Y, IMAGE_ID("titleguide")[0], true);
		if (animCnt < 255)
		{
			animCnt += 5;
		}
		else
		{
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
			animCnt += 10;
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, animCnt - 500);

			// 画面端まで来たかどうかを返す
			auto Menu = [=](int num, int slide) {
				if (animCnt - (TITLE_MENU_CHIP_Y * num) < screen.x)
				{
					DrawGraph(animCnt - TITLE_MENU_CHIP_X - (TITLE_MENU_CHIP_Y * num) - slide, screen.y - TITLE_MENU_CHIP_X + (TITLE_MENU_CHIP_Y * num), IMAGE_ID("titlemenu")[num], true);
					TitleAnimEnd = true;
				}
				else
				{
					DrawGraph(screen.x - TITLE_MENU_CHIP_X - slide, screen.y - TITLE_MENU_CHIP_X + (TITLE_MENU_CHIP_Y * num), IMAGE_ID("titlemenu")[num], true);
					if (num == static_cast<int>(PLAYER_NUM::RULES))
					{
						animEndFlag = true;
					}
					if (Mode == num)
					{
						DrawGraph(screen.x - TITLE_MENU_CHIP_X - 16 - 32, screen.y - TITLE_MENU_CHIP_X + (TITLE_MENU_CHIP_Y * num), IMAGE_ID("menucursor")[0], true);
					}
				}
			};

			if (Mode == static_cast<int>(PLAYER_NUM::START))
			{
				Menu(static_cast<int>(PLAYER_NUM::NON), 0);
				Menu(static_cast<int>(PLAYER_NUM::START), 16);
				Menu(static_cast<int>(PLAYER_NUM::RULES), 0);
			}
			else
			{
				Menu(static_cast<int>(PLAYER_NUM::NON), 0);
				Menu(static_cast<int>(PLAYER_NUM::START), 0);
				Menu(static_cast<int>(PLAYER_NUM::RULES), 16);
			}

			if (TitleAnimEnd)
			{
				lpSoundMan.PlayLoopSound(lpSoundMan.GetSoundId("title01", "wav")[0]);
			}

			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
	}
	else
	{
		DrawGraph(0, 0, IMAGE_ID("rules")[0], false);
	}

	// ここまで

	if (SceneChangeFlag)
	{
		SetDrawBright(sceneCnt, sceneCnt, sceneCnt);
	}

	ScreenFlip();		//  ｹﾞｰﾑﾙｰﾌﾟの最後に必ず必要
	return;
}


bool TitleScene::CheckSceneMove(void)
{
	sceneCnt -= 3;

	// BGMの減衰
	if (lpSoundMan.PlayLoopSound(lpSoundMan.GetSoundId("title01", "wav")[0]))
	{
		ChangeVolumeSoundMem(sceneCnt, lpSoundMan.GetSoundId("title01", "wav")[0]);
	}
	if (lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("select01", "mp3")[0], false))
	{
		ChangeVolumeSoundMem(sceneCnt, lpSoundMan.GetSoundId("select01", "mp3")[0]);
	}

	// ｶｳﾝﾀｰ(画面の明るさ)が0になってｼｰﾝ移行可能かどうか
	if (sceneCnt <= 0)
	{
		return true;
	}
	return false;
}
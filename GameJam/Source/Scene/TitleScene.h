#pragma once
#include <list>
#include <memory>
#include "BaseScene.h"
#include "Enum/PlayerNum.h"
#include "Game/GameController.h"
#include "Game/Func/ListObj.h"
#include "Game/Obj.h"

class TitleScene :
	public BaseScene
{
public:
	TitleScene();
	~TitleScene();

	Unique_Base Update(Unique_Base Own, const GameController &GameContObj);
private:
	int Init(void);

	void Draw();						// 描画関数
	bool CheckSceneMove(void);			// ｼｰﾝ移行して良いかどうか <true>:ｼｰﾝ移行, <false>:まだ移行しない

	bool SceneChangeFlag;				// ｼｰﾝ移行出来るかどうか <true>:ｼｰﾝ移行が1回でも実行された, <false>:ｼｰﾝ移行がまだ実行されていない
	int animCnt;						// ｱﾆﾒｰｼｮﾝｶｳﾝﾀｰ
	int sceneCnt;						// ｼｰﾝ移行までのｶｳﾝﾀｰ
	unsigned int Mode;					// 選択項目 Mode == 1 -> 1PlayerMode, Mode == 2 -> tutorial
	bool animEndFlag;					// ｼｰﾝ移行の暗転処理が終わったかどうか <true>:終了, <false>:まだ
	bool ViewRulesFlag;					// ﾙｰﾙの表示ﾓｰﾄﾞかどうか <true>:ﾙｰﾙを表示, <false>:ﾙｰﾙを非表示
	bool ViewRulesOldFlag;
	bool oneloop;						// ｼｰﾝ移行時に1回だけ音を再生するために使用(苦肉の策)
	bool TitleAnimEnd;					// ﾀｲﾄﾙ表示時に1回だけ音を再生するために使用(苦肉の策)
};
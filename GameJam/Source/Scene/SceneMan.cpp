#include "SceneMan.h"
#include "ResourceManager/ImageMan.h"
#include "GameScene.h"
#include "Game/GameController.h"
#include "TitleScene.h"
#include "Dxlib.h"

#define SCREEN_SIZE_X 800
#define SCREEN_SIZE_Y 600

#define SCREEN_OFFSET_X 40
#define SCREEN_OFFSET_Y 55

#define GAMESCREEN_SIZE_X 640
#define GAMESCREEN_SIZE_Y 480

#define GAMESCREEN_SIZE_X 640
#define GAMESCREEN_SIZE_Y 480

#define GRIDSIZE 44

SceneMan::SceneMan()
{
	SysInit();
}


SceneMan::~SceneMan()
{
}

VECTOR2 SceneMan::GetScreenSize()
{
	return VECTOR2(SCREEN_SIZE_X,SCREEN_SIZE_Y);
}

bool SceneMan::SetDrawOffSet(VECTOR2 vec)
{
	DrawOffset.x=vec.x;
	DrawOffset.y=vec.y;
	return true;
}

const VECTOR2 SceneMan::GetDrawOffSet(void)
{
	return DrawOffset;
}


void SceneMan::Run(void)
{
	ActiveScenePtr = std::make_unique<TitleScene>();																// ここでGameSceneを生成して移行
	while (ProcessMessage()  ==  0 && CheckHitKey(KEY_INPUT_ESCAPE)  ==  0)
	{
		GameControllerObj->Update();																			// 毎フレーム、最初にGameControllerを更新
		ActiveScenePtr = std::move( ActiveScenePtr->Update( std::move(ActiveScenePtr) ,*GameControllerObj) );	// ActiveScenePtrに登録されているSceneのUpdateを呼び出す
	}
}

const VECTOR2 SceneMan::GetMapSize(void)
{
	return MapSize;
}

const VECTOR2 SceneMan::GetGridSize(void)
{
	return VECTOR2(GRIDSIZE,GRIDSIZE);
}

//  ｹﾞｰﾑ系の初期化
bool SceneMan::SysInit(void)
{
	//  ｼｽﾃﾑ処理
	SetGraphMode(SCREEN_SIZE_X, SCREEN_SIZE_Y, 16);										//  65536色ﾓｰﾄﾞに設定
	ChangeWindowMode(true);																//  true:window　false:ﾌﾙｽｸﾘｰﾝ
	SetWindowText("SugarBeet");

	if (DxLib_Init()  ==  -1) return false;												//  DXﾗｲﾌﾞﾗﾘ初期化処理

	SetDrawScreen(DX_SCREEN_BACK);														//  ひとまずﾊﾞｯｸﾊﾞｯﾌｧに描画
	SetDrawOffSet( VECTOR2(SCREEN_OFFSET_X, SCREEN_OFFSET_Y) );
	
	MapSize = VECTOR2( GAMESCREEN_SIZE_X/GRIDSIZE, GAMESCREEN_SIZE_Y/GRIDSIZE );		// MapSizeを設定

	GameControllerObj = std::make_unique<GameController>();								// GameContlollerを生成

	return true;
}

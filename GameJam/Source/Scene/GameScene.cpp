#include "GameScene.h"
#include "SceneMan.h"
#include "Game/Func/ListObj.h"
#include "TitleScene.h"
#include "Game/EffectDraw.h"
#include "Enum/PlayerStatus.h"
#include "ResourceManager/SoundMan.h"
#include "Game/Obj.h"
#include "DxLib.h"

#define SCREEN_OFFSET_X 40			// ﾀｲﾄﾙ画面のｵﾌｾｯﾄ(X)
#define SCREEN_OFFSET_Y 75			// ﾀｲﾄﾙ画面のｵﾌｾｯﾄ(Y)

#define GAME_OFFSET_X	60			// ﾌｨｰﾙﾄﾞ描画中のｵﾌｾｯﾄ(X)
#define GAME_OFFSET_Y	40			// ﾌｨｰﾙﾄﾞ描画中のｵﾌｾｯﾄ(Y)

#define WAIT_CNT		300			// 最初のさいころが落ちてくるまでの時間
#define END_CNT			280			// ﾀｲﾄﾙへ移行するまでの時間


GameScene::GameScene()
{
	Init();
}


GameScene::~GameScene()
{
	SetDrawBright(255, 255, 255);
}

Unique_Base GameScene::Update(Unique_Base Own, const GameController & GameControllerObj)
{
	if (Player1->GetPlayerStatus() == PlayerStatus::NORMAL)
	{
		lpSoundMan.PlayLoopSound(lpSoundMan.GetSoundId("game01", "wav")[0]);
		if (waitCnt == 0)
		{
			Player1->Update(GameControllerObj);
			ChangeVolumeSoundMem(255, lpSoundMan.GetSoundId("game01", "wav")[0]);
		}
		else
		{
			waitCnt--;
			ChangeVolumeSoundMem(230, lpSoundMan.GetSoundId("game01", "wav")[0]);
		}
		
	}
	else
	{
		//こっちがゲームオーバー、クリア後の画面の入力受付です
		if (GameControllerObj.GetInputDown(KEY_INPUT_RETURN) && endCnt >= END_CNT)
		{
			return std::make_unique<TitleScene>();
		}
		endCnt++;
	}
	GameScene::Draw();
	return std::move(Own);
}


int GameScene::Init()
{
	Player1	= std::make_unique<Player>(VECTOR2(0,0));

	Player1->SetKeyConfig
	(
		KEY_INPUT_UP,
		KEY_INPUT_DOWN,
		KEY_INPUT_LEFT,
		KEY_INPUT_RIGHT,
		KEY_INPUT_RETURN,
		KEY_INPUT_Z,
		KEY_INPUT_X
	);

	ObjDraw = std::make_unique<GameDraw>();

	ObjDraw->Init();

	waitCnt = WAIT_CNT;
	endCnt	= 0;
	return 0;
}

void GameScene::Draw()
{
	ClsDrawScreen();																		// 画面消去

	ObjDraw->Draw(VECTOR2(GAME_OFFSET_X, GAME_OFFSET_Y ));

	// ここまで
	if (waitCnt == 0)
	{
		Player1->Draw(VECTOR2(GAME_OFFSET_X, GAME_OFFSET_Y));
	}
	ScreenFlip();		//  ｹﾞｰﾑﾙｰﾌﾟの最後に必要
	return;
}

#pragma once
#include <list>
#include <memory>
#include "BaseScene.h"
#include "Game/GameController.h"
#include "Game/Player.h"
#include "Game/Func/ListObj.h"
#include "Game/Obj.h"
#include "Game/DiceNum.h"

class GameScene :
	public BaseScene
{
public:
	GameScene();
	~GameScene();
	Unique_Base Update(Unique_Base Own, const GameController &GameContObj);
private:
	int Init();							// シーンの初期化
	void Draw();						// 描画関数

	//変数
	std::unique_ptr<Player>Player1;
	std::unique_ptr<GameDraw>ObjDraw;

	int waitCnt;							// 待機用変数
	int endCnt;
	//メソッド

};


#pragma once

enum class PlayerStatus
{
	NORMAL,					//プレイ中
	GAMECLEAR,				//ゲームクリア
	GAMEOVER,				//ゲームオーバー
	MAX
};
#pragma once


enum class DIR
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
	MAX
};
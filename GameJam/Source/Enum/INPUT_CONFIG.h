#pragma once
enum class INPUT_CONFIG
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
	ENTER,
	L_ROT,
	R_ROT,
	CONFIG_MAX,
};
#include "VECTOR2.h"

VECTOR2::VECTOR2()
{
}

VECTOR2::VECTOR2(int x, int y)
{
	this->x = x;
	this->y = y;
}


VECTOR2::~VECTOR2()
{
}

VECTOR2 & VECTOR2::operator=(const VECTOR2 & vec)
{
	this->x = vec.x;
	this->y = vec.y;
	return *this;
}

int& VECTOR2::operator[](int i)
{
	if (i  ==  0)
	{
		return this->x;
	}
	else if (i  ==  1)
	{
		return this->y;
	}
	else
	{
		return this->x;
	}
}

bool VECTOR2::operator == (const VECTOR2 & vec) const
{
	return ( (this->x == vec.x) && (this->y == vec.y) );
}

bool VECTOR2::operator!=(const VECTOR2 & vec) const
{
	return !( (this->x  ==  vec.x) && (this->y  ==  vec.y) );
}

VECTOR2 & VECTOR2::operator+=(const VECTOR2 & vec)
{
	this->x = this->x+vec.x;
	this->y = this->y+vec.y;
	return *this;
}

VECTOR2 & VECTOR2::operator-=(const VECTOR2 & vec)
{
	this->x = this->x - vec.x;
	this->y = this->y - vec.y;
	return *this;
}

VECTOR2 & VECTOR2::operator*=(int k)
{
	this->x = this->x * k;
	this->y = this->y * k;

	return *this;
}

VECTOR2 & VECTOR2::operator/=(int k)
{
	this->x = this->x / k;
	this->y = this->y / k;
	return *this;
}

VECTOR2 VECTOR2::operator++() const
{
	VECTOR2 tmp;
	tmp.x = this->x;
	tmp.y = this->y;
	tmp.x++;
	tmp.y++;
	return tmp;
}

VECTOR2 VECTOR2::operator--() const
{
	VECTOR2 tmp;
	tmp.x = this->x;
	tmp.y = this->y;
	tmp.x--;
	tmp.y--;
	return tmp;
}

VECTOR2 operator+(const VECTOR2& u, int v)
{
	VECTOR2 tmp;
	tmp.x = u.x + v;
	tmp.y = u.y + v;
	return tmp;
}

VECTOR2 operator+(const VECTOR2 & u, const VECTOR2 & v)
{
	VECTOR2 tmp;
	tmp.x = u.x + v.x;
	tmp.y = u.y + v.y;
	return tmp;
}

VECTOR2 operator-(const VECTOR2 & u, const VECTOR2 & v)
{
	VECTOR2 tmp;
	tmp.x = u.x - v.x;
	tmp.y = u.y - v.y;
	return tmp;
}

VECTOR2 operator*(const VECTOR2 & u, const VECTOR2 & v)
{
	VECTOR2 tmp;
	tmp.x = u.x * v.x;
	tmp.y = u.y * v.y;
	return tmp;
}

VECTOR2 operator%(const VECTOR2 & u, const VECTOR2 & v)
{
	VECTOR2 tmp;
	tmp.x = u.x % v.x;
	tmp.y = u.y % v.y;
	return tmp;
}

VECTOR2 operator/(const VECTOR2 & u, const VECTOR2 & v)
{
	VECTOR2 tmp;
	tmp.x = u.x / v.x;
	tmp.y = u.y / v.y;
	return tmp;
}

VECTOR2 operator*(int k, const VECTOR2 & v)
{
	VECTOR2 tmp;
	tmp.x = v.x * k;
	tmp.y = v.y * k;
	return tmp;
}

VECTOR2 operator*(const VECTOR2 & v, int k)
{
	VECTOR2 tmp;
	tmp.x = v.x * k;
	tmp.y = v.y * k;
	return tmp;
}

VECTOR2 operator/(const VECTOR2 & v, int k)
{
	VECTOR2 tmp;
	tmp.x = v.x / k;
	tmp.y = v.y / k;
	return tmp;
}

bool operator<(const VECTOR2 & v,int k)
{
	bool tmp = (v.x < k && v.y < k);
	return tmp;
}

bool operator>(const VECTOR2 & v,int k)
{
	bool tmp = (v.x > k&&v.y > k);
	return tmp;
}

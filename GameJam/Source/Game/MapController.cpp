#include "MapController.h"
#include "Scene/SceneMan.h"
#include "ResourceManager/SoundMan.h"


// 送られてきた座標が範囲内かどうかチェックする（グリッド指定）
struct CheckSize
{
	bool operator()(const VECTOR2 GridPos)
	{
		if (GridPos.x >= 0
			&&
			GridPos.x < PuzzleSize_X
			&&
			GridPos.y >= 0
			&&
			GridPos.y < PuzzleSize_Y)
		{
			return true;
		}
		return false;
	};
};


MapController::MapController()
{
	BlockTargetPos=
	{
		VECTOR2(0 ,-1),			//上
		VECTOR2(0 , 1),			//下
		VECTOR2(-1, 0),			//左
		VECTOR2(1 , 0)			//右
	};

}


MapController::~MapController()
{
}

bool MapController::SetUp(void)
{
	lpImageMan.GetImageId("Dice", VECTOR2(44, 44), VECTOR2(4, 2));		//Dice画像の読み込み

	auto CreateMap = [=](auto& Base, auto& LinkData, auto InitData)
	{
		for (unsigned i = 0; i<LinkData.size(); i++)
		{
			LinkData[i] = &Base[i*PuzzleSize_Y];						// LinkDataからBaseにLinkData[X][Y]でアクセスできるようにリンクさせる
		}

		for (unsigned int i = 0; i < Base.size(); i++)
		{
			Base[i] = InitData;										//初期化
		}
	};

	PuzzleDataBase.resize(PuzzleSize_X*PuzzleSize_Y+1);						// PuzzleSizeから必要分の要素数を確保する
	PuzzleData.resize(PuzzleSize_X);

	PuzzleEraseDataBase.resize(PuzzleSize_X*PuzzleSize_Y + 1);				// PuzzleSizeから必要分の要素数を確保する
	PuzzleEraseData.resize(PuzzleSize_X);

	CreateMap(PuzzleDataBase, PuzzleData,DICE_NUM::DICE_0);
	CreateMap(PuzzleEraseDataBase, PuzzleEraseData,false);

	DeletingFlag = false;

	return true;
}

void MapController::SetBlock(VECTOR2 SetPos, DICE_NUM SetId)
{
	PuzzleData[SetPos.x][SetPos.y] = SetId;
}

DICE_NUM MapController::GetBlock(VECTOR2 GetPos)
{
	return GetData(PuzzleData, GetPos, DICE_NUM::DICE_0);
}

VECTOR2 MapController::GetMapSize(void)
{
	return VECTOR2(PuzzleSize_X,PuzzleSize_Y);
}

int MapController::CheckDelete(int Combo)
{
	int ReturnSum = 0;
	DeletingFlag = false;

	for (int x = 0; x < PuzzleSize_X; x++)
	{
		for (int y = 0; y < PuzzleSize_Y; y++)
		{
			if (static_cast<int>(PuzzleData[x][y]) > 0)
			{
				VECTOR2 NowPos(x, y);
				int Sum = 0;

				auto SetEraseFrag = [&](DIR StartDir,DIR EndDir) 
				{
					if (Sum == 10)
					{
						SetData(PuzzleEraseData, NowPos, true);
						SetData(PuzzleEraseData, NowPos + BlockTargetPos[static_cast<int>(StartDir)], true);
						SetData(PuzzleEraseData, NowPos + BlockTargetPos[static_cast<int>(EndDir)], true);
						DeletingFlag = true;
					}
				};

				auto CheckErase = [&](DIR StartDir, DIR EndDir)
				{
					Sum = static_cast<int>(GetData(PuzzleData, NowPos, DICE_NUM::DICE_0));
					for (int i = static_cast<int>(StartDir); i <= static_cast<int>(EndDir); i++)
					{
						if (static_cast<int>(GetData(PuzzleData, NowPos + BlockTargetPos[i], DICE_NUM::DICE_0)) > 0)
						{
							Sum += static_cast<int>(GetData(PuzzleData, NowPos + BlockTargetPos[i], DICE_NUM::DICE_0));
						}
						else
						{
							Sum = -1;
							break;
						}
					}
					SetEraseFrag(StartDir,EndDir);
				};

				CheckErase(DIR::UP	 , DIR::DOWN);
				CheckErase(DIR::LEFT, DIR::RIGHT);
			}
		}
	}

	if (DeletingFlag == true)
	{
		Combo += 1;
		return DeleteBlock(Combo);
	}

	return 0;
}

void MapController::Draw(VECTOR2 Offset)
{
	VECTOR2 GridSize = lpSceneMng.GetGridSize();
	for (int x = 0; x < PuzzleSize_X; x++)
	{
		for (int y = 0; y < PuzzleSize_Y; y++)
		{
			DrawGraph(x*GridSize.x + Offset.x, y*GridSize.y + Offset.y, IMAGE_ID("Dice")[static_cast<int>(PuzzleData[x][y])], true);
		}
	}
}

void MapController::DownBlock(void)
{
	bool DownFlag = false;

	for (int x = 0; x < PuzzleSize_X; x++)
	{
		for (int y = PuzzleSize_Y -2; y > -1; y--)
		{

			if (PuzzleData[x][y]!=DICE_NUM::DICE_0 && PuzzleData[x][y + 1] == DICE_NUM::DICE_0)
			{
				int y2 = y;
				DownFlag = true;
				while (y2 < PuzzleSize_Y-1)
				{
					if (PuzzleData[x][y2+1]== DICE_NUM::DICE_0)
					{
						PuzzleData[x][y2 + 1] = PuzzleData[x][y2];
						PuzzleData[x][y2] = DICE_NUM::DICE_0;
						y2++;
					}
					else
					{
						break;
					}
				}

			}

		}
	}

	if (DownFlag == true)
	{
		lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("set01", "wav")[0], true);			// 設置
	}
}

int MapController::DeleteBlock(int Combo)
{
	int Sum = 0;
	for (int x = 0; x < PuzzleSize_X; x++)
	{
		for (int y = 0; y < PuzzleSize_Y; y++)
		{
			if (static_cast<bool>(PuzzleEraseData[x][y]) == true)
			{
				Sum += static_cast<int>(PuzzleData[x][y])*Combo;
				PuzzleData[x][y] = DICE_NUM::DICE_0;
				DeletingFlag = true;
			}
			PuzzleEraseData[x][y] = 0;
		}
	}
	return Sum;
}


bool MapController::GetDeletingFlag(void)
{
	return DeletingFlag;
}

void MapController::SetDeletingFlag(bool SetFlag)
{
	DeletingFlag = SetFlag;
}

bool MapController::CheckGameOver(void)
{
	bool ReturnFlag = false;

	for (int x = 0; x < PuzzleSize_X; x++)
	{
		if (PuzzleData[x][0]!=DICE_NUM::DICE_0)
		{
			return true;
		}
	}

	return false;
}

bool MapController::CheckDownBlock(void)
{
	for (int x = 0; x < PuzzleSize_X; x++)
	{
		for (int y = PuzzleSize_Y - 2; y > -1; y--)
		{

			if (PuzzleData[x][y] != DICE_NUM::DICE_0 && PuzzleData[x][y + 1] == DICE_NUM::DICE_0)
			{

				return true;
			}

		}
	}
	return false;
}

template<typename MapType, typename IdType>
bool MapController::SetData(MapType Maps, const VECTOR2 & SetPos, const IdType SetId)
{
	if (CheckSize()(SetPos) == true)
	{
		Maps[SetPos.x][SetPos.y] = SetId;
		_RPTN(_CRT_WARN, "write[%d][%d]_%x\n", SetPos.x, SetPos.y, SetId);
		return true;
	}
	return false;
}

template<typename MapType, typename IdType>
IdType MapController::GetData(MapType Maps, const VECTOR2 & GetPos, IdType DefId)
{
	if (CheckSize()(GetPos) == true)							// 範囲外チェック
	{
		return Maps[GetPos.x][GetPos.y];								// 範囲内なら指定座標のMapIdを返す
	}
	else
	{
		return DefId;													// 範囲外ならREDWALLのIdを返す
	}
}
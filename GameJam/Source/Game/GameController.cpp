#include "GameController.h"



GameController::GameController()
{
	OldMouseInputFlag = 0;
	NowMouseInputFlag = 0;
	GetMousePoint(&MousePos.x, &MousePos.y);
}


GameController::~GameController()
{
}

const HitKeyArray& GameController::GetNowInput() const
{
	return HitKeyNowArray;
}

const HitKeyArray& GameController::GetOldInput() const
{
	return HitKeyOldArray;
}

const int & GameController::GetNowMouseInputFlag() const
{
	return NowMouseInputFlag;
}

const int & GameController::GetOldMouseInputFlag() const
{
	return OldMouseInputFlag;
}

const VECTOR2 & GameController::GetMousePos() const
{
	return MousePos;
}

void GameController::Update()
{
	// Keyの入力を更新する
	RefreshInput();
}

const bool GameController::GetNowInputKey(int InputKeyCode) const
{
	return HitKeyNowArray[InputKeyCode] == 1;
}

const bool GameController::GetOldInputKey(int InputKeyCode) const
{
	return HitKeyOldArray[InputKeyCode] == 1;
}

bool GameController::GetInputDown(int InputKeyCode) const
{
	// 前フレームが0で、現フレームが1の場合ボタンが押し込まれていると判断できる
	// どのキーを判別するかは引数のKeyCodeを用いる
	if (HitKeyNowArray[InputKeyCode] & ~HitKeyOldArray[InputKeyCode])
	{
		return true;
	}

	return false;
}

bool GameController::GetInputHold(int InputKeyCode) const
{
	// 前フレームが1で、現フレームが1の場合ボタンが押し込まれ続けていると判断できる
	// どのキーを判別するかは引数のKeyCodeを用いる
	if (HitKeyNowArray[InputKeyCode] & HitKeyOldArray[InputKeyCode])
	{
		return true;
	}
	return false;
}

bool GameController::GetInputUp(int InputKeyCode) const
{
	// 前フレームが1で、現フレームが0の場合ボタンが離された瞬間と判断できる
	// どのキーを判別するかは引数のKeyCodeを用いる
	if ( ~HitKeyNowArray[InputKeyCode] & HitKeyOldArray[InputKeyCode])
	{
		return true;
	}
	return false;
}

bool GameController::GetMouseInputDown(int InputKeyCode) const
{
	// 前フレームが0で、現フレームが1の場合ボタンが押し込まれていると判断できる
	// どのキーを判別するかは引数のKeyCodeを用いる
	if ( (NowMouseInputFlag & InputKeyCode) & ~(OldMouseInputFlag & InputKeyCode) )
	{
		return true;
	}
	return false;
}

bool GameController::GetMouseInputHold(int InputKeyCode) const
{
	// 前フレームが1で、現フレームが1の場合ボタンが押し込まれ続けていると判断できる
	// どのキーを判別するかは引数のKeyCodeを用いる
	if ( (NowMouseInputFlag & InputKeyCode) & (OldMouseInputFlag & InputKeyCode) )
	{
		return true;
	}
	return false;
}

bool GameController::GetMouseInputUp(int InputKeyCode) const
{
	// 前フレームが1で、現フレームが0の場合ボタンが離された瞬間と判断できる
	// どのキーを判別するかは引数のKeyCodeを用いる
	if ( ~(NowMouseInputFlag & InputKeyCode) & (OldMouseInputFlag & InputKeyCode) )
	{
		return true;
	}
	return false;
}

void GameController::RefreshInput(void)
{
	// KeyArrayの更新
	HitKeyOldArray = HitKeyNowArray;
	GetHitKeyStateAll(&HitKeyNowArray[0]);
	GetMousePoint(&MousePos.x, &MousePos.y);
	// MouseInputFlagの更新
	OldMouseInputFlag = NowMouseInputFlag;
	NowMouseInputFlag = GetMouseInput();
}
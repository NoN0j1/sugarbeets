#pragma once
#include <list>
#include <memory>
#include <array>
#include "DiceNum.h"
#include "VECTOR2.h"
#include "MapController.h"

// さいころが落ち始めてから描画する物はここで
class EffectDraw
{
public:
	EffectDraw(std::weak_ptr <MapController> MapCont);
	~EffectDraw();

	void Init(int clearScore);								//初期化、絶対最初に呼び出して。

	void SceneDraw(VECTOR2 Offset,int& score, int& combo, bool delTiming);	//描画
	void ClearDraw(VECTOR2 Offset);							//描画
	void OverDraw(VECTOR2 Offset);							//描画
private:
	// 変数
	int SceneCnt;									// ｼｰﾝ中ののｶｳﾝﾀｰ
	int endCnt;										// その他のｶｳﾝﾄ
	int clearScore;									// ｸﾘｱのｽｺｱ
	bool timeAddFlag;								// 時間のﾘﾐｯﾄ

	std::array<int, 3> timeCnt;						// 時間表示用
	std::array<int, 5> ScoreCnt;					// ｽｺｱの格納
};


#pragma once
#include "ResourceManager/ImageMan.h"
#include "GameController.h"
#include "Game/Func/ListObj.h"
#include <memory>
#include <map>
#include <string>

enum ANIM_TBL 
{
	ANIM_TBL_START_ID,		// 開始フレーム位置
	ANIM_TBL_FRAME,			// フレーム数
	ANIM_TBL_INV,			// 更新間隔
	ANIM_TBL_LOOP,			// ループを行うかどうか
	ANIM_TBL_MOVE,			// フレームの進む方向
	ANIM_TBL_MAX
};

enum ANIM_LOOP
{
	ANIM_LOOP_ON,			// ループアニメーション
	ANIM_LOOP_OFF,			// シングルアニメーション
	ANIM_LOOP_MAX
};

// アニメーションのコマ送りの方向
enum ANIM_MOVE
{
	ANIM_MOVE_RIGHT,		// 指定位置から右へ
	ANIM_MOVE_LEFT,			// 指定位置から左へ
	ANIM_MOVE_UP,			// 指定位置から上へ
	ANIM_MOVE_DOWN,			// 指定位置から下へ
	ANIM_MOVE_MAX
};

#define ANIM_FRAME_X 0
#define ANIM_FRAME_Y 1

class VECTOR2;

class Obj
{
	// 基底クラスなので、比較的汎用性の高い機能のみを搭載しています
public:
	Obj(VECTOR2 DrawOffset);
	Obj();
	virtual ~Obj();

	bool Init(std::string FileName, VECTOR2 DivSize, VECTOR2 DivCnt);				// ロードする画像、分割サイズ、分割数を設定できる
	bool Init(std::string FileName, VECTOR2 DivSize, VECTOR2 DivCnt,VECTOR2 Pos);	// Initの座標設定用引数あり版

	bool SetObjList(WeakListObj GetObjList);					// ObjListのWeakPointerをセットする
	void SetObjTag(std::string Tag);							// ObjectTagを設定する

	virtual void Update(const GameController & GameContObj);						
	virtual void Draw();															

	const VECTOR2& GetPos(void);								// キャラクターの座標を送るゲッター
	const VECTOR2& GetSize(void);								// キャラクターのサイズを送るゲッター

	//アニメーション追加

	bool AddAnim(std::string AnimName,			// アニメーション名
		VECTOR2 StartAnimPos,					// 開始コマ
		int AnimFrame,							// コマ数
		int AnimInv);							// フレーム間隔


	bool AddAnim(std::string AnimName,			// アニメーション名
		VECTOR2 StartAnimPos,					// 開始コマ
		int AnimFrame,							// コマ数
		int AnimInv,							// フレーム間隔
		ANIM_MOVE AnimMove);					// フレーム送り方向

	bool AddAnim(std::string AnimName,			// アニメーション名
				 VECTOR2 StartAnimPos,			// 開始コマ
				 int AnimFrame,					// コマ数
				 int AnimInv,					// フレーム間隔
				 ANIM_MOVE AnimMove,			// フレーム送り方向
				 ANIM_LOOP AnimLoop);			// アニメーションのループ

	bool SetAnim( std::string AnimName );		// アニメーション名を指定して、アニメーション切り替え
	std::string  GetAnim(void);					// 今のアニメーション名を返す

	void SetDestroyFlag(bool SetBool);			// DestroyFlagのセッター
	bool CheckDestroyFlag(void);				// DestroyFlagを返す

	std::string GetObjectTag();					// 判別用のTagを返す

	// 矩形当たり判定　相手のタグ、ポジション、サイズを取得して判定する、当たってたらTrueが帰る
	void CheckHit(std::string OtherTag,VECTOR2 OtherPos,VECTOR2 OtherSize);

protected:
	std::string ObjectTag;							// オブジェクトの判別用タグ
	virtual bool CheckTag(std::string OtherTag);	// *仮想関数　当たり判定時に、当たった相手が当たるべき相手なのかをチェックする

	VECTOR2 ScreenSize;								// 画面サイズ
	VECTOR2 DrawOffset;								// 描画位置のオフセット

	VECTOR2 Pos;									// キャラクターの座標
	void SetPos(VECTOR2 SetPos);					// 送られてきたVECTOR2を座標に反映させる

	std::string  ImageName;							// キャラクターの使用するスプライト名
	VECTOR2 DivSize;								// スプライトの分割サイズ
	VECTOR2 DivCnt;									// スプライトの分割数

	std::string  AnimName;							// 使用するアニメーション名
	bool AnimEnd;									// アニメーションが終了時にTrue
	unsigned AnimCnt;								// アニメーションカウンタ
	virtual void AnimInit() {};						// *仮想関数　アニメーションの初期設定

	WeakListObj ObjList;							// インスタンスされているオブジェクトが格納されているリストへのWeakPointer

private:
	std::map  <std::string, int[ANIM_TBL_MAX]> AnimTable;			// 読み込んだアニメーション画像をどのようにして扱うかをアニメーション名とともに保存されるMap
	bool DestroyFlag = false;										// このオブジェクトを破棄するときにTrueにする
	void AnimIdSet(int& Id);										// アニメーションの描画フレームを設定する
};


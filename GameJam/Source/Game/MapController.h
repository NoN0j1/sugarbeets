#pragma once
#include <list>
#include <memory>
#include "Func/ListObj.h"
#include "Obj.h"
#include "DiceNum.h"
#include "Scene/SceneMan.h"
#include "Enum/DIR.h"
#include "VECTOR2.h"

#define PuzzleSize_X 6
#define PuzzleSize_Y 12

class MapController
{
public:
	MapController();
	~MapController();

	bool SetUp(void);

	void	 SetBlock(VECTOR2 SetPos,DICE_NUM SetId);
	DICE_NUM GetBlock(VECTOR2 GetPos);

	VECTOR2 GetMapSize(void);

	int CheckDelete(int Combo);

	bool GetDeletingFlag(void);								//削除モードかを判別するためのフラグゲッター
	void SetDeletingFlag(bool SetFlag);

	bool CheckGameOver(void);								//最上段にBlockがセットされているかチェックする　されていたらtrue(ゲームオーバー)
	bool CheckDownBlock(void);								//何処かに落下予定のブロックがあるかどうかチェックする　あるならTrue

	void DownBlock(void);

	void Draw(VECTOR2 Offset);

private:
	std::vector<DICE_NUM>	PuzzleDataBase;				// PuzzleMapのデータを保存するVector　SetUpにて動的にMapSizeのX,Y+1を掛けた分確保される
	std::vector<DICE_NUM*>	PuzzleData;					// PuzzleMapDataBaseを二次元配列としてアクセスできるようにポインターを格納するVector

	std::vector<int>	PuzzleEraseDataBase;				// PuzzleMapのデータを削除するフラグを保存するVector　SetUpにて動的にMapSizeのX,Y+1を掛けた分確保される
	std::vector<int*>	PuzzleEraseData;					// PuzzleEraseDataBaseを二次元配列としてアクセスできるようにポインターを格納するVector

	std::array<VECTOR2, static_cast<int>(DIR::MAX)> BlockTargetPos;		//上下左右のブロックの数値を取得するためのターゲット

	bool DeletingFlag;										//削除フェーズに入っているかどうか	Trueだと削除フェーズ

	template<typename MapType, typename IdType>
	bool SetData(MapType Maps, const VECTOR2 & SetPos, const IdType SetId);

	template<typename MapType, typename IdType>
	IdType GetData(MapType Maps, const VECTOR2 & SetPos, IdType DefId);

	//メソッド
	int DeleteBlock(int Combo);
};


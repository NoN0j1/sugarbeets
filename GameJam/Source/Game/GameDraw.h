#pragma once
#include <list>
#include <memory>
#include "DiceNum.h"
#include "VECTOR2.h"
#include "MapController.h"

class GameDraw
{
public:
	GameDraw();
	~GameDraw();

	void Init(void);										//初期化、絶対最初に呼び出して。

	void Draw(VECTOR2 Offset);								//描画
private:
	int blendCnt;									// ｼｰﾝ切り替え時のﾌﾞﾚﾝﾄﾞ用
	int frameCnt;									// ｹﾞｰﾑ中のｶｳﾝﾀｰ
	bool startFlag;									
};


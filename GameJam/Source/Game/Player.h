#pragma once
#include <mutex>
#include <list>
#include <memory>
#include "Enum/DIR.h"
#include "DiceNum.h"
#include "Enum/INPUT_CONFIG.h"
#include "VECTOR2.h"
#include "MapController.h"
#include "GameDraw.h"
#include "GameController.h"
#include "BlockEditor.h"
#include "EffectDraw.h"
#include "Enum/PlayerStatus.h"
#include "DxLib.h"

class Player
{
public:
	Player(VECTOR2 Offset);
	~Player();

	void SetKeyConfig(int Up,int Down,int Left,int Right,int Enter,int L_ROT,int R_ROT);

	void Update(const GameController & GameContObj);


	PlayerStatus GetPlayerStatus(void);

	void Draw(VECTOR2 Offset);
private:
	std::array<int, static_cast<int>(INPUT_CONFIG::CONFIG_MAX)> KeyConfig;
	VECTOR2 Offset;

	PlayerStatus NowStatus;

	std::shared_ptr<MapController> MapCont;
	std::shared_ptr<BlockEditor> DropBlock;
	std::shared_ptr<EffectDraw> DrawObj;		// さいころが落ち始めてから描画する物

	int FrameCnt;

	int Score;									//Playerのスコアです、描画するときとか、クリア判定とかに使います
	int LevelUpScore;
	int	 DeleteCombo;							//連鎖確認用

	int MoveSpeed;

	bool DeleteTiming;
	bool clearFlag;		// 音用
	//メソッド
	void Init(VECTOR2 Offset);

	void MainGameInput(const GameController & GameContObj);

	void GameEndInput(const GameController & GameContObj);

};


#pragma once
#include <array>

class ScoreBoard
{
public:

	//シングルトンにするので、必ずこの関数を通してアクセスします。
	static ScoreBoard& Instance()
	{
		static ScoreBoard instance;
		return instance;
	}

	//プレイ結果を引数で送り、スコアデータに登録します。
	//この時、ランキング外であれば　-1　を　ランキング内であればその順位が返り値で帰ります。
	int SetScoreData(int setScore);

	const std::array<int, 5>& GetScoreData();

	//起動後、最後にプレーしたプレイヤーの順位を返します。
	int GetLastPlayerRank();

private:
	ScoreBoard();
	~ScoreBoard();
	void operator=(const ScoreBoard&);

	//スコアのデータを格納する変数。
	std::array<int,5> ScoreData;

	//最後にプレーされた時のスコアの順位が保存されます。
	//ランキング外であれば-1が格納されています。
	int LastPlayerRank;

	//保存されてるスコアデータをDatから読み取ります。
	void LoadScoreData();

	//スコアデータをDatに書き出します。
	void WriteScoreData();
};


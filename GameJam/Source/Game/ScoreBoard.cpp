#include "ScoreBoard.h"
#include "DxLib.h"
#include <string>

#pragma warning(disable: 4996)

const std::string Path = "ScoreData/Data.dat";


ScoreBoard::ScoreBoard()
{
	ScoreData = {0, 0, 0, 0, 0};
	LastPlayerRank = -1;
	LoadScoreData();
	int Test = 00;
}


ScoreBoard::~ScoreBoard()
{
}

int ScoreBoard::SetScoreData(int setScore)
{
	LastPlayerRank = -1;

	for (int TargetNum=4;TargetNum>=0;TargetNum--)
	{
		if (ScoreData[TargetNum]<setScore)
		{
			LastPlayerRank = TargetNum;
		}
		else
		{
			break;
		}
	}
	
	if (LastPlayerRank != -1)
	{
		for (int TargetNum=3;TargetNum>=LastPlayerRank;TargetNum--)
		{
			ScoreData[TargetNum + 1] = ScoreData[TargetNum];
		}
		ScoreData[LastPlayerRank] = setScore;
	}

	//��������
	WriteScoreData();

	return LastPlayerRank;
}

const std::array<int, 5>& ScoreBoard::GetScoreData()
{
	return ScoreData;
}

int ScoreBoard::GetLastPlayerRank()
{
	return LastPlayerRank;
}

void ScoreBoard::LoadScoreData()
{
	int DataH = FileRead_open(Path.c_str());
	FileRead_read(&ScoreData[0],ScoreData.size(),DataH);
	FileRead_close(DataH);
}

void ScoreBoard::WriteScoreData()
{
	FILE *DataH = fopen(Path.c_str(),"wb");	
	fwrite(&ScoreData[0],ScoreData.size(),1,DataH);
	fclose(DataH);
}



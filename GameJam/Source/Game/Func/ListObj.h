#pragma once
#include <list>
#include <memory>

/*
リストへのオブジェクトの追加などに使用する際などにIncludeして使用してください
*/

class Obj;

using SharedObj = std::shared_ptr<Obj>;
using SharedObjList = std::list<SharedObj>;
using Shared_ListItr = SharedObjList::iterator;
using Shared_ListObj = std::shared_ptr<SharedObjList>;
using WeakListObj = std::weak_ptr<SharedObjList>;

struct AddObjList_Back
{
	// ObjListにObjをPushBackで追加する
	Shared_ListItr operator()(WeakListObj ObjList, SharedObj AddObj)
	{
		ObjList.lock()->push_back( std::move(AddObj) );			// リストの末端にObjを追加
		Shared_ListItr Itr = ObjList.lock()->end();				// イテレーターをEndに動かす
		Itr--;													// Endから一つイテレーターを戻す(実態のある場所へ移動させる)
		return Itr;												// イテレーターを返す
	}
};

struct AddObjList_Front
{
	// ObjListにObjをPushFrontで追加する
	Shared_ListItr operator()(WeakListObj ObjList, SharedObj AddObj)
	{
		ObjList.lock()->push_front( std::move(AddObj) );			// リストの末端にObjを追加
		Shared_ListItr Itr = ObjList.lock()->begin();			// イテレーターをBeginに動かす
		return Itr;												// イテレーターを返す
	}
};
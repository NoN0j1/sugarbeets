#pragma once
#include <string>
#include "VECTOR2.h"
#include "DxLib.h"

struct DrawStr
{
	//DxLibのDrawFormatStringに座標と文字列、色を一括入力できる関数
	void operator() (std::string str, VECTOR2 DrawPos, int red, int green, int blue)
	{
	int TextColor = GetColor(red, green, blue);
	DrawFormatString( DrawPos.x, DrawPos.y, TextColor, str.c_str() );
	}
};

struct DrawStr_w
{
	//座標と文字列を送れば白文字でその場所に表示してくれる
	void operator() (std::string str,VECTOR2 DrawPos)
	{
		int TextColor = GetColor(255,255,255);
		DrawFormatString( DrawPos.x, DrawPos.y, TextColor, str.c_str() );
	}
};

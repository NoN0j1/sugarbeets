#include "EffectDraw.h"
#include "Player.h"
#include "ScoreBoard.h"
#include <cassert>

EffectDraw::EffectDraw(std::weak_ptr <MapController> MapCont)
{
}


EffectDraw::~EffectDraw()
{
}

void EffectDraw::Init(int clearScore)
{
	auto font_path = "font/GAGAGAGA.otf";
	if (AddFontResourceEx(font_path, FR_PRIVATE, NULL) < 0)
	{
		//Μ«έΔΗέέ΄Χ°
		assert("fontLoad Error");
	}
	ChangeFont("GAGAGAGA FREE", DX_CHARSET_DEFAULT);
	SetFontSize(50);
	lpImageMan.GetImageId("finish");
	lpImageMan.GetImageId("clear");
	lpImageMan.GetImageId("over");
	lpImageMan.GetImageId("touch");
	lpImageMan.GetImageId("score_back",VECTOR2(285,76),VECTOR2(1,10));
	lpImageMan.GetImageId("combo");
	lpImageMan.GetImageId("comboFont", { 72, 72 }, {5,2});
	lpImageMan.GetImageId("rank", { 60,60 }, { 5,1 });
	lpImageMan.GetImageId("ranking");
	lpImageMan.GetImageId("gameover");
	lpImageMan.GetImageId("rankframe");
	lpImageMan.GetImageId("scoreText");

	SceneCnt	= 1;
	endCnt		= 0;
	timeAddFlag	= true;
	timeCnt		= {
		0,		// bΜ1Ϊ
		0,		// bΜ2Ϊ
		0		// ͺΜ1Ϊ
	};
	ScoreCnt	= {
		0,		// 1Ϊ
		-1,		// 2Ϊ
		-1,		// 3Ϊ
		-1,		// 4Ϊ
		-1,		// 5Ϊ
	};
	EffectDraw::clearScore = clearScore;
}

void EffectDraw::SceneDraw(VECTOR2 Offset,int& score, int& combo, bool delTiming)
{	
	if (SceneCnt % 60 == 0)
	{
		timeCnt[0]++;
		if (timeCnt[0] % 10 == 0)
		{
			timeCnt[1]++;
			timeCnt[0] = 0;
			if (timeCnt[1] % 6 == 0)
			{
				timeCnt[2]++;
				timeCnt[1] = 0;
				if (timeCnt[2] > 9)
				{
					timeCnt[2]	= 9;
					timeAddFlag	= false;
				}
			}
		}
	}
	
	if (timeAddFlag)
	{
		ScoreCnt[4] = score / 10000;
		ScoreCnt[3] = score / 1000;
		ScoreCnt[2] = score / 100;
		ScoreCnt[1] = (score - (ScoreCnt[2] * 100)) / 10;
		ScoreCnt[0] = score - ((ScoreCnt[2] * 100) + (ScoreCnt[1] * 10));
		if (SceneCnt < 35939)
		{
			//^C}[γΐ9ͺ59b
			SceneCnt++;
		}
	}
	else
	{
		timeCnt[0] = 9;
		timeCnt[1] = 5;
		timeCnt[2] = 9;
	}
	
	DrawGraph(Offset.x + 265 + 64 * 3 + 32, Offset.y + 92, IMAGE_ID("font")[timeCnt[0]], true);
	DrawGraph(Offset.x + 265 + 64 * 2 + 32, Offset.y + 92, IMAGE_ID("font")[timeCnt[1]], true);	
	DrawGraph(Offset.x + 265 + 64 + 48, Offset.y + 92, IMAGE_ID("font")[10], true);				
	DrawGraph(Offset.x + 329, Offset.y + 92, IMAGE_ID("font")[timeCnt[2]], true);
	DrawGraph(Offset.x + 329, Offset.y + 210, IMAGE_ID("scoreText")[0], true);
	auto scoreDraw = [&](int no) {
		if (no > 4 || no < 0)
		{
			return;
		}
		if (ScoreCnt[no] != -1)
		{
			DrawGraph(Offset.x + 520 - no*48, Offset.y + 260, IMAGE_ID("font")[ScoreCnt[no]], true);
		}
	};
	scoreDraw(0);
	scoreDraw(1);
	scoreDraw(2);
	scoreDraw(3);
	scoreDraw(4);

	if (combo >= 2 && delTiming)
	{
		DrawGraph(Offset.x + 150 + 50, Offset.y + 292, IMAGE_ID("comboFont")[combo], true);		// ΊέΞήΜ
		DrawGraph(Offset.x + 150 + 50 + 80, Offset.y + 292, IMAGE_ID("combo")[0], true);		// ΊέΞή
	}
}

void EffectDraw::ClearDraw(VECTOR2 Offset)
{
	if (endCnt < 90)
	{
		DrawGraph(200, 250, IMAGE_ID("finish")[0], true);
	}
	if(endCnt >= 200)
	{
		SetDrawBright(255, 255, 255);
		DrawGraph(100, 250, IMAGE_ID("clear")[0], true);
		if (endCnt % 130 <= 100)
		{
			DrawGraph(200, 400, IMAGE_ID("touch")[0], true);
		}
		SetDrawBright(100, 100, 100);
	}
	endCnt++;
}

void EffectDraw::OverDraw(VECTOR2 Offset)
{
	SetDrawBright(255, 255, 255);
	if (endCnt < 90)
	{
		DrawGraph(200, 250, IMAGE_ID("finish")[0], true);
	}
	if (endCnt  >= 150)
	{
		SetDrawBright(255 - 3 * (endCnt - 150), 255 - 3 * (endCnt - 150), 255 - 3 * (endCnt - 150));
	}
	if (endCnt > 270)
	{
		auto scores = ScoreBoard::Instance().GetScoreData();
		SetDrawBright(255, 255, 255);
		DrawGraph(0, 0, IMAGE_ID("gameover")[0], true);

		DrawGraph(180, 20, IMAGE_ID("ranking")[0], true);
		DrawGraph(100, 100, IMAGE_ID("rankframe")[0], true);
		for (int i = 0; i < scores.size(); i++)
		{
			DrawGraph(200, 120 + 70 * i, IMAGE_ID("rank")[i], true);
			DrawFormatString(300, 130 + 70 * i, 0xffffff, "%d", scores[i]);
		}

		if (endCnt % 130 <= 100)
		{
			DrawGraph(200, 500, IMAGE_ID("touch")[0], true);
		}
		SetDrawBright(0, 0, 0);
	}
	endCnt++;
}

#pragma once
#include <type_traits>

enum class DICE_NUM
{
	DICE_0,
	DICE_1,
	DICE_2,
	DICE_3,
	DICE_4,
	DICE_5,
	DICE_6,
	DICE_MAX,
};

DICE_NUM begin(DICE_NUM);

DICE_NUM end(DICE_NUM);

DICE_NUM operator++(DICE_NUM& dicenum);
DICE_NUM operator--(DICE_NUM& dicenum);
DICE_NUM operator*(DICE_NUM& dicenum);
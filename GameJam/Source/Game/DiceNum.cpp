#include <type_traits>
#include "DiceNum.h"

DICE_NUM begin(DICE_NUM)
{
	return DICE_NUM::DICE_0;
}

DICE_NUM end(DICE_NUM)
{
	return DICE_NUM::DICE_MAX;
}

DICE_NUM operator++(DICE_NUM& dicenum)
{
	return dicenum = DICE_NUM(std::underlying_type<DICE_NUM>::type(dicenum) + 1);
}

DICE_NUM operator--(DICE_NUM& dicenum)
{
	return dicenum = DICE_NUM(std::underlying_type<DICE_NUM>::type(dicenum) - 1);
}

DICE_NUM operator*(DICE_NUM& dicenum)
{
	return dicenum;
}
#include "Obj.h"
#include "Dxlib.h"
#include "Scene/SceneMan.h"
Obj::Obj()
{
	Pos.x = 0;
	Pos.y = 0;
}

Obj::Obj(VECTOR2 Offset)
{
	Pos.x = 0;
	Pos.y = 0;

	DrawOffset = Offset;
}


Obj::~Obj()
{
}

bool Obj::Init(std::string FileName, VECTOR2 DivSize, VECTOR2 DivCnt)
{
	lpImageMan.GetImageId(FileName,DivSize,DivCnt);

	this->ImageName = FileName;
	this->DivSize   = DivSize;
	this->DivCnt    = DivCnt;

	ScreenSize = SceneMan::GetInstans().GetScreenSize();

	AnimEnd = false;
	AnimInit();

	return true;
}

bool Obj::Init(std::string FileName, VECTOR2 DivSize, VECTOR2 DivCnt, VECTOR2 Pos)
{
	Init(FileName, DivSize, DivCnt);
	SetPos(Pos);
	return true;
}

bool Obj::SetObjList(WeakListObj GetObjList)
{
	ObjList = GetObjList;
	return true;
}

void Obj::SetObjTag(std::string Tag)
{
	ObjectTag = Tag;
}


void Obj::Update(const GameController & GameContObj)
{
}

void Obj::Draw()
{
	if (ImageName.length()  ==  0)
	{
		return;
	}
	int Id = 0;
	if (AnimName.length() != 0)
	{
		AnimIdSet(Id);
	}
	DrawGraph(Pos.x+DrawOffset.x, Pos.y+DrawOffset.y, IMAGE_ID(ImageName)[Id], true);
	AnimCnt++;
}

const VECTOR2 &Obj::GetPos(void)
{
	return Pos;
}

const VECTOR2 & Obj::GetSize(void)
{
	return DivSize;
}

bool Obj::AddAnim(std::string AnimName, VECTOR2 StartAnimPos, int AnimFrame, int AnimInv)
{
	AddAnim(AnimName,StartAnimPos,AnimFrame,AnimInv,ANIM_MOVE_RIGHT,ANIM_LOOP_ON);
	return true;
}

bool Obj::AddAnim(std::string AnimName, VECTOR2 StartAnimPos, int AnimFrame, int AnimInv, ANIM_MOVE AnimMove)
{
	AddAnim(AnimName, StartAnimPos, AnimFrame, AnimInv,AnimMove, ANIM_LOOP_ON);
	return true;
}

bool Obj::AddAnim(std::string AnimName, VECTOR2 StartAnimPos, int AnimFrame, int AnimInv,ANIM_MOVE AnimMove, ANIM_LOOP AnimLoop)
{
	AnimTable[AnimName][ANIM_TBL_START_ID]=(StartAnimPos.y*DivCnt.x)+StartAnimPos.x;
	AnimTable[AnimName][ANIM_TBL_FRAME] =AnimFrame;
	AnimTable[AnimName][ANIM_TBL_INV] = AnimInv;
	AnimTable[AnimName][ANIM_TBL_LOOP] = AnimLoop;
	AnimTable[AnimName][ANIM_TBL_MOVE] = AnimMove;
	return true;
}

bool Obj::SetAnim(std::string AnimName)
{
	if (this->AnimName  ==  AnimName)
	{
		return true;
	}

	if ( AnimTable.find(AnimName)  ==  AnimTable.end() )
	{
		return false;
	}
	AnimEnd = false;
	this->AnimName = AnimName;
	AnimCnt = 0;
	return true;
}

std::string Obj::GetAnim(void)
{
	return AnimName;
}

void Obj::SetDestroyFlag(bool SetBool)
{
	DestroyFlag = SetBool;
}

bool Obj::CheckDestroyFlag(void)
{
	return DestroyFlag;
}

std::string Obj::GetObjectTag()
{
	return ObjectTag;
}

void Obj::CheckHit(std::string OtherTag, VECTOR2 OtherPos, VECTOR2 OtherSize)
{
	if ((Pos.x < OtherPos.x + OtherSize.x)
	  &&(OtherPos.x < Pos.x + DivSize.x)
	  &&(Pos.y < OtherPos.y + OtherSize.y)
	  &&(OtherPos.y < Pos.y + DivSize.y)
	  &&(CheckTag(OtherTag)  ==  true)
	   )
	{
		SetDestroyFlag(true);
	}
}

void Obj::SetPos(VECTOR2 SetPos)
{
	Pos.x = SetPos.x;
	Pos.y = SetPos.y;
	return;
}

void Obj::AnimIdSet(int & Id)
{
	switch (AnimTable[AnimName][ANIM_TBL_MOVE])
	{
	case ANIM_MOVE_RIGHT:
		Id = AnimTable[AnimName][ANIM_TBL_START_ID] + ((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) % AnimTable[AnimName][ANIM_TBL_FRAME]);
		if ((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) >= (unsigned)AnimTable[AnimName][ANIM_TBL_FRAME] - 1
			&&
			AnimTable[AnimName][ANIM_TBL_LOOP] == ANIM_LOOP_OFF)
		{
			Id = AnimTable[AnimName][ANIM_TBL_START_ID] + AnimTable[AnimName][ANIM_TBL_FRAME] - 1;
		}
		break;
	case ANIM_MOVE_LEFT:
		Id = AnimTable[AnimName][ANIM_TBL_START_ID] - ((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) % AnimTable[AnimName][ANIM_TBL_FRAME]);
		if ((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) >= (unsigned)AnimTable[AnimName][ANIM_TBL_FRAME] - 1
			&&
			AnimTable[AnimName][ANIM_TBL_LOOP] == ANIM_LOOP_OFF)
		{
			Id = AnimTable[AnimName][ANIM_TBL_START_ID] - AnimTable[AnimName][ANIM_TBL_FRAME] - 1;
		}
		break;
	case ANIM_MOVE_UP:
		Id = AnimTable[AnimName][ANIM_TBL_START_ID] - DivCnt.x*((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) % AnimTable[AnimName][ANIM_TBL_FRAME]);
		if ((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) >= (unsigned)AnimTable[AnimName][ANIM_TBL_FRAME] - 1
			&&
			AnimTable[AnimName][ANIM_TBL_LOOP] == ANIM_LOOP_OFF)
		{
			Id = AnimTable[AnimName][ANIM_TBL_START_ID] - DivCnt.x*(AnimTable[AnimName][ANIM_TBL_FRAME] - 1);
		}
		break;
	case ANIM_MOVE_DOWN:
		Id = AnimTable[AnimName][ANIM_TBL_START_ID] + DivCnt.x*((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) % AnimTable[AnimName][ANIM_TBL_FRAME]);
		if ((AnimCnt / AnimTable[AnimName][ANIM_TBL_INV]) >= (unsigned)AnimTable[AnimName][ANIM_TBL_FRAME] - 1
			&&
			AnimTable[AnimName][ANIM_TBL_LOOP] == ANIM_LOOP_OFF)
		{
			Id = AnimTable[AnimName][ANIM_TBL_START_ID] + DivCnt.x*(AnimTable[AnimName][ANIM_TBL_FRAME] - 1);
			AnimEnd = true;
		}
		break;
	case ANIM_MOVE_MAX:
		break;
	default:
		break;
	}
}

bool Obj::CheckTag(std::string OtherTag)
{
	return false;
}



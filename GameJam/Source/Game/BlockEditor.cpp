#include <Time.h>
#include "ResourceManager/SoundMan.h"
#include "BlockEditor.h"

#define NEXT_BLOCK_POS_X	329
#define NEXT_BLOCK_POS_Y	13

BlockEditor::BlockEditor(std::weak_ptr <MapController> MapCont)
{
	srand((unsigned int)time(NULL));

	MyMapCont = MapCont;

	//落ちるブロックの初期化
	DropingBlock[0] = { DICE_NUM::DICE_0,DICE_NUM::DICE_0 };
	DropingBlock[1] = { DICE_NUM::DICE_0,DICE_NUM::DICE_0 };

	//NEXTの初期化
	NextDropingBlock[0] = { DICE_NUM::DICE_0,DICE_NUM::DICE_0 };
	NextDropingBlock[1] = { DICE_NUM::DICE_0,DICE_NUM::DICE_0 };

	//NEXTの決定
	NextDropingBlock[0] = { MakeDice(),DICE_NUM::DICE_0 };
	NextDropingBlock[1] = { MakeDice(),DICE_NUM::DICE_0 };

	VECTOR2 GridSize = lpSceneMng.GetGridSize();
	BlockPos = VECTOR2(2, 0);
	DrawPos = VECTOR2(BlockPos.x*GridSize.x,0);

	SetDropingBlock();
}


BlockEditor::~BlockEditor()
{
}

void BlockEditor::Init(void)
{
	BlockPos=VECTOR2(2,0);
}

bool BlockEditor::RotR(void)
{
	auto Tmp= DropingBlock;
	VECTOR2 NowPos = DrawPos / lpSceneMng.GetGridSize();

	Tmp[1][0] = DropingBlock[0][0];
	Tmp[1][1] = DropingBlock[1][0];
	Tmp[0][1] = DropingBlock[1][1];
	Tmp[0][0] = DropingBlock[0][1];

	for (int y=0;y<2;y++)
	{
		if (Tmp[1][y] != DICE_NUM::DICE_0&&Tmp[0][y] == DICE_NUM::DICE_0)
		{
			Tmp[0][y] = Tmp[1][y];
			Tmp[1][y] = DICE_NUM::DICE_0;
		}
	}

	if (Tmp[1][0]!= DICE_NUM::DICE_0 && (MyMapCont.lock()->GetBlock(VECTOR2(NowPos.x +1, NowPos.y)) != DICE_NUM::DICE_0|| NowPos.x + 1 >= PuzzleSize_X))
	{
		NowPos.x -= 1;
	}

	for (int x = 0; x < 2; x++)
	{
		for (int y = 0; y < 2; y++)
		{
			if (Tmp[x][y] != DICE_NUM::DICE_0)
			{
				if (NowPos.x + x >= PuzzleSize_X || NowPos.y + y >= PuzzleSize_Y)
				{
					return false;
				}


				if (MyMapCont.lock()->GetBlock(VECTOR2(NowPos.x + x, NowPos.y + y)) != DICE_NUM::DICE_0)
				{
					return false;
				}
			}
		}
	}

	DropingBlock=Tmp;
	BlockPos = NowPos;
	DrawPos.x = BlockPos.x*lpSceneMng.GetGridSize().x;
	return true;
}

bool BlockEditor::RotL(void)
{
	auto Tmp = DropingBlock;
	VECTOR2 NowPos = DrawPos / lpSceneMng.GetGridSize();

	Tmp[0][0] = DropingBlock[1][0];
	Tmp[0][1] = DropingBlock[0][0];
	Tmp[1][1] = DropingBlock[0][1];
	Tmp[1][0] = DropingBlock[1][1];

	for (int y = 0; y<2; y++)
	{
		if (Tmp[1][y] != DICE_NUM::DICE_0&&Tmp[0][y] == DICE_NUM::DICE_0)
		{
			Tmp[0][y] = Tmp[1][y];
			Tmp[1][y] = DICE_NUM::DICE_0;
		}
	}

	if (Tmp[1][1] != DICE_NUM::DICE_0 && (MyMapCont.lock()->GetBlock(VECTOR2(NowPos.x + 1, NowPos.y+1)) != DICE_NUM::DICE_0 || NowPos.x + 1 >= PuzzleSize_X))
	{
		NowPos.x -= 1;
	}

	for (int x = 0; x < 2; x++)
	{
		for (int y = 0; y < 2; y++)
		{
			if (Tmp[x][y] != DICE_NUM::DICE_0)
			{
				if (NowPos.x + x >= PuzzleSize_X || NowPos.y + y >= PuzzleSize_Y)
				{
					return false;
				}


				if (MyMapCont.lock()->GetBlock(VECTOR2(NowPos.x + x, NowPos.y + y)) != DICE_NUM::DICE_0)
				{
					return false;
				}
			}
		}
	}

	DropingBlock = Tmp;
	BlockPos = NowPos;
	DrawPos.x = BlockPos.x*lpSceneMng.GetGridSize().x;

	return true;
}

bool BlockEditor::MoveR(void)
{
	VECTOR2 NextPos = BlockPos;
	NextPos.x += 1;

	for (int x = 0; x < 2; x++)
	{
		for (int y = 0; y < 2; y++)
		{
			if (DropingBlock[x][y] != DICE_NUM::DICE_0)
			{
				if (NextPos.x + x >= PuzzleSize_X)
				{
					return false;
				}

				if (MyMapCont.lock()->GetBlock(VECTOR2(NextPos.x+x, NextPos.y + y)) != DICE_NUM::DICE_0)
				{
					return false;
				}
			}
		}
	}

	BlockPos = NextPos;
	VECTOR2 GridSize = lpSceneMng.GetGridSize();
	DrawPos = VECTOR2(BlockPos.x*GridSize.x, DrawPos.y);

	return true;
}

bool BlockEditor::MoveL(void)
{
	VECTOR2 NextPos = BlockPos;
	NextPos.x -= 1;

	for (int x = 0; x < 2; x++)
	{
		for (int y = 0; y < 2; y++)
		{
			if (DropingBlock[x][y] != DICE_NUM::DICE_0)
			{
				if (NextPos.x+x<0)
				{
					return false;
				}
				if (MyMapCont.lock()->GetBlock(VECTOR2(NextPos.x + x, NextPos.y + y)) != DICE_NUM::DICE_0)
				{
					return false;
				}
			}
		}
	}

	BlockPos = NextPos;
	VECTOR2 GridSize = lpSceneMng.GetGridSize();
	DrawPos = VECTOR2(BlockPos.x*GridSize.x, DrawPos.y);

	return true;
}

void BlockEditor::HardDrop(void)
{
	SetBlock();
	MyMapCont.lock()->DownBlock();
}

void BlockEditor::BlockDown(int Speed)
{
	VECTOR2 NextDrawPos = DrawPos;
	NextDrawPos.y += Speed;

	VECTOR2 NextPos = NextDrawPos / lpSceneMng.GetGridSize();
	NextPos.y += 1;
	for (int x = 0; x < 2; x++)
	{
		for (int y = 0; y < 2; y++)
		{
			if (DropingBlock[x][y] != DICE_NUM::DICE_0)
			{
				if (NextPos.y + y>=PuzzleSize_Y)
				{
					SetBlock();
					lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("set01", "wav")[0], true);			// 設置
					return;
				}
				if (MyMapCont.lock()->GetBlock(VECTOR2(NextPos.x + x, NextPos.y + y)) != DICE_NUM::DICE_0)
				{
					SetBlock();
					lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("set01", "wav")[0], true);			// 設置
					return;
				}
			}
		}
	}

	DrawPos = NextDrawPos;
	BlockPos = NextPos;
}


void BlockEditor::Draw(VECTOR2 Offset)
{
	VECTOR2 GridSize = lpSceneMng.GetGridSize();
	for (int x=0;x<2;x++)
	{
		for (int y = 0; y < 2; y++)
		{
			//操作中のブロックの表示
			if (DropingBlock[x][y] != DICE_NUM::DICE_0)
			{
				//DrawGraph((BlockPos.x + x) * GridSize.x + Offset.x, (BlockPos.y + y) * GridSize.y + Offset.y, IMAGE_ID("Dice")[static_cast<int>(DropingBlock[x][y])], true);
				DrawGraph(DrawPos.x+ (x* GridSize.x )+ Offset.x, DrawPos.y + (y * GridSize.y )+ Offset.y, IMAGE_ID("Dice")[static_cast<int>(DropingBlock[x][y])], true);
			}
		}
	}
}

void BlockEditor::NextDraw(VECTOR2 Offset)
{
	VECTOR2 GridSize = lpSceneMng.GetGridSize();
	for (int x = 0; x<2; x++)
	{
		//次のブロックの表示
		if (NextDropingBlock[x][0] != DICE_NUM::DICE_0)
		{
			DrawGraph( x * GridSize.x + Offset.x + NEXT_BLOCK_POS_X,Offset.y + NEXT_BLOCK_POS_Y, IMAGE_ID("Dice")[static_cast<int>(NextDropingBlock[x][0])], true);
		}
	}
}

void BlockEditor::SetDropingBlock(void)
{
	VECTOR2 GridSize = lpSceneMng.GetGridSize();

	BlockPos = VECTOR2(2,0);
	DrawPos = VECTOR2(BlockPos.x*GridSize.x,0);
	DropingBlock = NextDropingBlock;
	NextDropingBlock[0] = {MakeDice(),DICE_NUM::DICE_0};
	NextDropingBlock[1] = { MakeDice(),DICE_NUM::DICE_0 };
}

DICE_NUM BlockEditor::MakeDice(void)
{
	int Rand = rand() % 10;
	DICE_NUM ReturnDice = DICE_NUM::DICE_0;

	switch (Rand)
	{
	case 0:
		ReturnDice = DICE_NUM::DICE_1;
		break;

	case 1:
	case 2:

		ReturnDice = DICE_NUM::DICE_2;
		break;

	case 3:
	case 4:

		ReturnDice = DICE_NUM::DICE_3;
		break;

	case 5:
	case 6:

		ReturnDice = DICE_NUM::DICE_4;
		break;

	case 7:
	case 8:

		ReturnDice = DICE_NUM::DICE_5;
		break;

	case 9:

		ReturnDice = DICE_NUM::DICE_6;
		break;

	default:
		break;
	}

	return ReturnDice;
}

void BlockEditor::SetBlock(void)
{
	for (int x = 0; x < 2; x++)
	{
		for (int y = 0; y < 2; y++)
		{
			if (DropingBlock[x][y]!=DICE_NUM::DICE_0)
			{
				MyMapCont.lock()->SetBlock(VECTOR2(BlockPos.x + x, BlockPos.y + y), DropingBlock[x][y]);
				DropingBlock[x][y] = DICE_NUM::DICE_0;
			}
		}
	}
	MyMapCont.lock()->SetDeletingFlag(true);
}

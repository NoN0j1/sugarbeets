#include "GameDraw.h"
#include "ResourceManager/SoundMan.h"

#define FIELD_WIDTH_X	27			// ﾌｨｰﾙﾄﾞのﾌﾚｰﾑの横幅
#define FIELD_WIDTH_Y	13			// ﾌｨｰﾙﾄﾞのﾌﾚｰﾑの縦幅

#define FIELD_SIZE_X	316			// NEXT画像の大きさ
#define START_SIZE_X	300			// スタートの大きさ

GameDraw::GameDraw()
{
}


GameDraw::~GameDraw()
{
}

void GameDraw::Init(void)
{
	lpImageMan.GetImageId("field");
	lpImageMan.GetImageId("next");
	lpImageMan.GetImageId("fieldline");
	lpImageMan.GetImageId("background");
	lpImageMan.GetImageId("startfont",VECTOR2(260,80),VECTOR2(1,2));
	lpImageMan.GetImageId("gamesceneguide");
	lpImageMan.GetImageId("time");
	lpImageMan.GetImageId("score");
	lpImageMan.GetImageId("font",VECTOR2(64,64),VECTOR2(5,3));
	lpImageMan.GetImageId("timeframe");

	blendCnt	= 0;
	frameCnt	= 0;
	startFlag	= false;
}

void GameDraw::Draw(VECTOR2 Offset)
{
	VECTOR2 screen = lpSceneMng.GetScreenSize();
	blendCnt += 3;
	if (blendCnt > 255)
	{
		frameCnt++;
	}
	else 
	{ 
		SetDrawBright(blendCnt, blendCnt, blendCnt);
	}

	DrawGraph(0,0,IMAGE_ID("background")[0],true);
	DrawGraph(Offset.x - FIELD_WIDTH_X, Offset.y - FIELD_WIDTH_Y, IMAGE_ID("field")[0], true);
	DrawGraph(Offset.x - FIELD_WIDTH_X, Offset.y - FIELD_WIDTH_Y, IMAGE_ID("fieldline")[0], true);
	DrawGraph(Offset.x + FIELD_SIZE_X, Offset.y, IMAGE_ID("next")[0], true);
	DrawGraph(Offset.x + FIELD_SIZE_X, Offset.y + 250, IMAGE_ID("score")[0], true);
	DrawGraph(Offset.x + FIELD_SIZE_X, Offset.y + 80, IMAGE_ID("time")[0], true);
	DrawGraph(500, 408, IMAGE_ID("gamesceneguide")[0], true);
	if(frameCnt < 20){ /*処理なし*/ }
	else if (frameCnt >= 20 && frameCnt <= 120)
	{
		DrawGraph(Offset.x, 250, IMAGE_ID("startfont")[0], true);
	}
	else if(frameCnt >= 130 && frameCnt <= 170)
	{
		DrawGraph(Offset.x + 15, 250, IMAGE_ID("startfont")[1], true);
		if (startFlag == false)
		{
			lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("start01", "wav")[0], true);
			startFlag = true;
		}
	}
}

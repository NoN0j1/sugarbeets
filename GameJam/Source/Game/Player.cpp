#include "Player.h"
#include "ResourceManager/SoundMan.h"
#include "ScoreBoard.h"

#define CLEARSCORE 250

void Player::Init(VECTOR2 Offset)
{
	MapCont = std::make_shared<MapController>();
	MapCont->SetUp();
	DropBlock = std::make_shared<BlockEditor>(MapCont);
	DropBlock->Init();
	DrawObj = std::make_shared<EffectDraw>(MapCont);
	DrawObj->Init(CLEARSCORE);
	FrameCnt = 1;
	clearFlag = false;
	Score = 0;
	LevelUpScore = 0;
	MoveSpeed = 1;
	DeleteCombo = 0;
	DeleteTiming = false;
	NowStatus = PlayerStatus::NORMAL;

    lpSoundMan.GetSoundId("Rote01", "wav");
    lpSoundMan.GetSoundId("anRote01", "wav");

    lpSoundMan.GetSoundId("Move01", "wav");
    lpSoundMan.GetSoundId("anMove01", "wav");

    lpSoundMan.GetSoundId("set01", "wav");
    lpSoundMan.GetSoundId("erase01", "wav");

}

void Player::MainGameInput(const GameController & GameContObj)
{
    bool NowModeFlag = MapCont->GetDeletingFlag();

    if (NowModeFlag != true)
    {
        if (GameContObj.GetInputDown(KeyConfig[static_cast<int>(INPUT_CONFIG::L_ROT)]))
        {
            if (DropBlock->RotL())
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("Rote01", "wav")[0], true);		//回転成功
            }
            else
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("anRote01", "wav")[0], true);		//回転失敗
            }
        }

        if (GameContObj.GetInputDown(KeyConfig[static_cast<int>(INPUT_CONFIG::R_ROT)]))
        {
            if(DropBlock->RotR())
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("Rote01", "wav")[0], true);		//回転完了
            }
            else
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("anRote01", "wav")[0], true);		//回転失敗
            }
        }


        if (GameContObj.GetInputDown(KeyConfig[static_cast<int>(INPUT_CONFIG::LEFT)]))
        {
            if(DropBlock->MoveL())
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("Move01", "wav")[0], true);			//移動完了
            }
            else
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("anMove01", "wav")[0], true);			//移動失敗
            }

        }

        if (GameContObj.GetInputDown(KeyConfig[static_cast<int>(INPUT_CONFIG::RIGHT)]))
        {
            if (DropBlock->MoveR())
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("Move01", "wav")[0], true);			//移動完了
            }
            else
            {
                lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("anMove01", "wav")[0], true);		//移動失敗
            }

        }

        if (GameContObj.GetInputHold(KeyConfig[static_cast<int>(INPUT_CONFIG::DOWN)]))
        {
            DropBlock->BlockDown(MoveSpeed*2);
        }
        else
        {
            DropBlock->BlockDown(MoveSpeed);
        }

        if (MapCont->GetDeletingFlag()!=true)
        {
            if (GameContObj.GetInputDown(KeyConfig[static_cast<int>(INPUT_CONFIG::UP)]))
            {
                DropBlock->HardDrop();
            }
        }

    }
    else
    {
        if (FrameCnt % 30 == 0)
        {
            if (DeleteTiming == false)
            {
                MapCont->DownBlock();
                DeleteTiming = true;
            }
            else
            {

				int UpScore = MapCont->CheckDelete(DeleteCombo);
				Score += UpScore;
				if (Score > 99999)
				{
					Score = 99999;	//スコア上限
				}
				LevelUpScore += UpScore;

				int LvUpCount = LevelUpScore / 100;

				if (LevelUpScore>0 && MoveSpeed<10)
				{
					LevelUpScore -= 100 * LvUpCount;
					MoveSpeed += LvUpCount;
				}

                DeleteTiming = false;
                if (MapCont->GetDeletingFlag() == true)
                {
                    DeleteCombo += 1;

                    lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("erase01","wav")[0], true);		//消えた時の音
                }
                else
                {
					if (MapCont->CheckDownBlock()==false  && MapCont->CheckGameOver() == true)
                    {
                        //ゲームオーバー
                        NowStatus = PlayerStatus::GAMEOVER;
						//スコア送信
						ScoreBoard::Instance().SetScoreData(Score);
                    }

                }
            }
        }

        if (MapCont->GetDeletingFlag() == false)
        {
            DropBlock->SetDropingBlock();
            DeleteTiming = false;
            DeleteCombo = 0;
        }
    }

    if (MapCont->GetDeletingFlag() == NowModeFlag)
    {
        FrameCnt++;
    }
    else
    {
        FrameCnt = 1;
    }
}

void Player::GameEndInput(const GameController & GameContObj)
{
    //ゲーム終了後の入力などを書いてください
}


Player::Player(VECTOR2 Offset)
{
    Init(Offset);
}


Player::~Player()
{
}

void Player::SetKeyConfig(int Up, int Down, int Left, int Right, int Enter, int L_ROT, int R_ROT)
{
    KeyConfig[static_cast<int>(INPUT_CONFIG::UP)]    = Up;
    KeyConfig[static_cast<int>(INPUT_CONFIG::DOWN)]  = Down;
    KeyConfig[static_cast<int>(INPUT_CONFIG::LEFT)]  = Left;
    KeyConfig[static_cast<int>(INPUT_CONFIG::RIGHT)] = Right;
    KeyConfig[static_cast<int>(INPUT_CONFIG::ENTER)] = Enter;
    KeyConfig[static_cast<int>(INPUT_CONFIG::L_ROT)] = L_ROT;
    KeyConfig[static_cast<int>(INPUT_CONFIG::R_ROT)] = R_ROT;
}

void Player::Update(const GameController & GameContObj)
{
    if (NowStatus==PlayerStatus::NORMAL)
    {
        MainGameInput(GameContObj);
    }
}


PlayerStatus Player::GetPlayerStatus(void)
{
    return NowStatus;
}

void Player::Draw(VECTOR2 Offset)
{
    switch (NowStatus)
    {
    case PlayerStatus::NORMAL:
        MapCont->Draw(Offset);
        DropBlock->NextDraw(Offset);
        DropBlock->Draw(Offset);
        DrawObj->SceneDraw(Offset,Score,DeleteCombo, DeleteTiming);
        break;
    case PlayerStatus::GAMECLEAR:
        MapCont->Draw(Offset);
        DropBlock->NextDraw(Offset);
        //ここから下に書いて
        DrawObj->ClearDraw(Offset);
        if (clearFlag == false)
        {
            StopSoundMem(lpSoundMan.GetSoundId("game01", "wav")[0]);
            lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("clear01", "wav")[0], false);
            clearFlag = true;
        }

        break;
    case PlayerStatus::GAMEOVER:
        MapCont->Draw(Offset);
        DropBlock->NextDraw(Offset);

        //ここから下に書いて
        DrawObj->OverDraw(Offset);
        if (clearFlag == false)
        {
            StopSoundMem(lpSoundMan.GetSoundId("game01", "wav")[0]);
            lpSoundMan.PlaySoundEffect(lpSoundMan.GetSoundId("feild01", "wav")[0], false);
            clearFlag = true;
        }

        break;
    case PlayerStatus::MAX:
    default:
        break;
    }

}

#pragma once
#include <list>
#include <memory>
#include "DiceNum.h"
#include "VECTOR2.h"
#include "MapController.h"

class BlockEditor
{
public:
	BlockEditor(std::weak_ptr <MapController> MapCont);
	~BlockEditor();

	void SetDropingBlock(void);								//落ちるブロックを生成、初期座標へ

	void Init(void);										//初期化、絶対最初に呼び出して。

	bool RotR(void);										//ブロック右回転		(できなければキャンセル)
	bool RotL(void);										//ブロック左回転		(できなければキャンセル)

	bool MoveR(void);										//ブロック右移動		(移動できなければキャンセル)
	bool MoveL(void);										//ブロック左移動		(移動できなければキャンセル)

	void HardDrop(void);									//即座に落とす

	void BlockDown(int Speed);								//ブロックが1マス下がる(下がれなかったらそこに設置)


	void Draw(VECTOR2 Offset);								//描画
	void NextDraw(VECTOR2 Offset);
private:
	//変数
	std::weak_ptr < MapController > MyMapCont;				//自分が関係しているMapControlle

	VECTOR2  BlockPos;										//ブロックのグリッド位置
	VECTOR2  DrawPos;										//描画位置

	std::array<std::array<DICE_NUM, 2>,2>  DropingBlock;	//落ちていくBlockのデータ
	std::array<std::array<DICE_NUM, 2>, 2> NextDropingBlock;

	//メソッド

	DICE_NUM MakeDice(void);								//落ちるブロックをランダムで決定

	void SetBlock(void);									//ブロックをMapに書き込む

};


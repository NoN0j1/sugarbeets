#include "ImageMan.h"
#include "DxLib.h"

const VEC_INT& ImageMan::GetImageId(std::string f_name)
{
	if ( ImageMap.find(f_name) == ImageMap.end() )
	{
		std::string FilePassString = "image/" + f_name + ".png";

		const char *FilePassChar = FilePassString.c_str();
		
		ImageMap[f_name].resize(1);
		ImageMap[f_name][0] = LoadGraph(FilePassChar);

	}
	return ImageMap[f_name];
}

const VEC_INT & ImageMan::GetImageId(std::string f_name, VECTOR2 divSize, VECTOR2 divCnt)
{
	if ( ImageMap.find(f_name) == ImageMap.end() )
	{
		std::string FilePassString = "image/" + f_name + ".png";

		const char *FilePassChar = FilePassString.c_str();

		ImageMap[f_name].resize(divCnt.x*divCnt.y);

		LoadDivGraph(FilePassChar,divCnt.x*divCnt.y,
					 divCnt.x,divCnt.y,divSize.x, divSize.y,&ImageMap[f_name][0]);
	}
	return ImageMap[f_name];
}



ImageMan::ImageMan()
{
}


ImageMan::~ImageMan()
{
}

#pragma once
#include  <mutex>
#include  <map>
#include  <vector>
#include "VECTOR2.h"

#define lpImageMan ImageMan::GetInstans()
#define IMAGE_ID(X) (ImageMan::GetInstans().GetImageId(X) )

using VEC_INT = std::vector<int>;

class VECTOR2;
class ImageMan
{
public:
	static ImageMan& GetInstans(void)												
	{
		static ImageMan s_ImageManInstance;
		return s_ImageManInstance;
	}
	const VEC_INT& GetImageId(std::string f_name);									// 分割なしの画像データを読み込む
	const VEC_INT& GetImageId(std::string f_name,VECTOR2 divSize,VECTOR2 DivCnt);	// 分割ありの画像データを読み込む

private:
	std::map  <std::string,VEC_INT> ImageMap;										// 読み込まれた画像を保存するMap

	ImageMan();
	~ImageMan();
};


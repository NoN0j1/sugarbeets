#pragma once
#include  <mutex>
#include  <map>
#include  <vector>

#define lpSoundMan  SoundMan::GetInstans()
#define SOUND_ID(X) (SoundMan::GetInstans().GetSoundId(X) )

using VEC_INT = std::vector<int>;

class SoundMan
{
public:
	static SoundMan& GetInstans(void)
	{
		static SoundMan s_ImageManInstance;
		return s_ImageManInstance;
	}

	const VEC_INT& GetSoundId(std::string f_name,std::string f_extension);			// 音声データを読み込む

	bool PlayLoopSound(const int& sound);					// ﾙｰﾌﾟﾀｲﾌﾟのｻｳﾝﾄﾞ再生に使用する (ﾊﾝﾄﾞﾙ)
															//<true>:音を既に再生していた <false>:その呼び出し時に再生した

	bool PlaySoundEffect(const int& sound, bool stop);		// 1回切りﾀｲﾌﾟのSE等のｻｳﾝﾄﾞ再生に使用する (ﾊﾝﾄﾞﾙ, 1回止めて再生し直すか)
															// <true>:音を既に再生していた <false>:呼び出し時に再生した、もしくは再生し直した

private:
	std::map  <std::string, VEC_INT> SoundMap;				// 読み込まれた音声を保存するMap

 SoundMan();
 ~SoundMan();
};


#include "SoundMan.h"
#include "DxLib.h"


const VEC_INT & SoundMan::GetSoundId(std::string f_name, std::string f_extension)
{
	if (SoundMap.find(f_name) == SoundMap.end())
	{
		std::string FilePassString = "sound/" + f_name + "."+f_extension;

		const char *FilePassChar = FilePassString.c_str();

		SoundMap[f_name].resize(1);
		SoundMap[f_name][0] = LoadSoundMem(FilePassChar);

	}
	return SoundMap[f_name];
}

SoundMan::SoundMan()
{
}


SoundMan::~SoundMan()
{
}


bool SoundMan::PlayLoopSound(const int& sound)
{
	if (CheckSoundMem(sound) == false)
	{
		ChangeVolumeSoundMem(255,sound);
		PlaySoundMem(sound, DX_PLAYTYPE_LOOP, true);
		return false;
	}
	return true;
}

bool SoundMan::PlaySoundEffect(const int& sound, bool stop)
{
	if (CheckSoundMem(sound) == false)
	{
		PlaySoundMem(sound, DX_PLAYTYPE_BACK, true);
		return false;
	}
	else if (stop)
	{
		StopSoundMem(sound);
		PlaySoundMem(sound, DX_PLAYTYPE_BACK, true);
		return false;
	}

	return true;
}

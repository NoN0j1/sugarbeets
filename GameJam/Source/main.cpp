// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
// // // // // // // // // // // // // // // // // // // // // // // / 「ゲーム制作」 基本ｿｰｽ
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
using namespace std;
#include <stdlib.h>
#include "Scene/SceneMan.h"
#include "Game/Obj.h"
#include "Dxlib.h"	//  DxLibﾗｲﾌﾞﾗﾘを使用する


//  ---------- WinMain関数
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	SceneMan::GetInstans().Run();
	DxLib_End();//  DXﾗｲﾌﾞﾗﾘの終了処理
	return 0;
}
